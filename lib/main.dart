import 'package:flutter/material.dart';
import 'package:times/pages/Home.dart';
import 'package:times/pages/ChooseLocation.dart';
import 'package:times/pages/Loading.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: "/",
    routes: {
      "/": (context) => Loading(),
      "/home": (context) => Home(),
      "/chooseLocation": (context) => ChooseLocation()
    },
  ));
}
